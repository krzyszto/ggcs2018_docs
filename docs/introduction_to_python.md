# Introduction to python
Run the python interpreter console, you should see something similar to this

```
Python 3.6.5 (v3.6.5:f59c0932b4, Mar 28 2018, 03:03:55) 
[GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```

## Hello World!

First build in function that you will learn is print. We use it to print texts to the console output. Functions take arguments, in this example we would use a string `'Hello World!'`
Execute following code:
```python
print('Hello World!')
```
Congratulations! You wrote your first program in python.


##Import this
We will use import now to see some of the rules and guidelines in python

Try typing following code:

```python
import this
```

You should see following text in the console

```
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```

This funny text is what describes how Pythonista (how python programmers are sometimes called) should approach the language itself.
Try to read it and even if you don't understand every line, don't worry, you'll get there.

## Import antigravity

Python comes with many build in libraries. Most of them are used for serious programming, but some have hidden funny context.

We used import before, let's do that again and see what will be result this time.

```python
import antigravity
```

## "Beautiful is better than ugly"

Python comes with a lots of standards, you should care that your code is beautiful.
There are many rules to follow and sometimes IDE helps us with that, there are also other tools that can help you, like for example flake8.
You can find all rules [here](https://www.python.org/dev/peps/pep-0008/). But go through them later, unfortunately we don't have time for that today.

## Cheatsheet

You can find a summary of python syntax [here](https://learnxinyminutes.com/docs/python3/)

## Exercises
Hacerrank is a website that stores many algorithmic exercises, create a account there. You can continue training your skills even after workshop is over.
* [Hello World!](https://www.hackerrank.com/challenges/py-hello-world/problem)
