#Installation

For our workshop, we will need a Python 3.6 interpreter. Below are some directions on how to check if you already have the interpreter and how to install it along with some other tools.
##Windows

You can download Python for Windows directly from python.org. After downloading the file *.msi, open it and follow the instructions. It is important to remember the path of installation – the directory – as we will need this information during the installation of tools.
##Linux (Ubuntu, Fedora, etc.) or Mac

In order to check our version of Python, enter the following in the command line:

```bash
$ python --version
Python 3.6.4
```

If the python command is not available or the wrong version appears:
###Ubuntu

Enter in the command line:

```bash
sudo apt-get install python3.6
```
###Fedora

Enter in the command line:

```bash
sudo yum install python3.6
```
###OS X

Download and install the package for your system version from python.org .
###Other

Use the system of packages adequate for your distribution. If there is no adequate system or you cannot find python, you can install it using the sources on the python.org. website. A compiler and readline library will be required.

Unofficially, we assume that the users of less popular (but not worse!) distributions will manage the task without any problem :).
##Tools
###Windows command line

We will do most of our work on the command line. To activate the command line in Windows, press Win+R. In the open window, type cmd and click OK. A new window will appear with white text on a black background:

```
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation. All rights reserved.

C:\Users\Name>
```
Text may be different depending on the version of Windows you use.

`C:\Users\Name>` is a prompt. It informs us of the directory (or folder) in which we currently are and waits for a command. Later during the workshop C:\Users\Name> we will refer to it with ~$, independently of your operating system (Windows, Linux, MacOS).


Using the command line, you can move around the contents of the disc (in the same way as in My Computer). You can do that by entering commands and pressing Enter. Use the following commands:

####dir
Displays the contents of the current directory. For example, if the prompt shows C:\Users\Name, the dir command displays the contents of our home directory.

####cd directory
Changes the current directory. For example, if you are in C:\Users\Name, you can access the directory with your documents by entering cd Documents. If you execute the dir command, you will see something familiar. The command cd.. will move you one level up in the directory tree, that is, to the directory that contains your current directory.
####mkdir directory
Creates a new directory.

###Virtual environment

Now we have to chose the directory for our virtual environment. The virtual environment will allow us to isolate our work from other parts of the system. For example, you can choose the user home directory.

For Windows 7 the path to the home directory for the user Ala will look like this: C:\Users\Ala\ . You can select a different directory, but it is important to remember where the file is saved. It must be easily accessible, because we will use it often.

For example, if our home directory is C:\Users\lrekucki, the command line would look like this:

####Windows
```
C:\Users\lrekucki> C:\Python36\python -m venv workshops
```
#### Linux or Mac
```
$ python3.6 -m venv workshops
```

In your home directory we will create a workshops directory containing the so called “virtual environment”. For now, it is important to activate it:


####Windows
```
C:\Users\lrekucki> workshops\Scripts\activate
```

####Linux or Mac
```
$ source workshops/bin/activate
```

The python command will run the correct version of Python, so we will not have to enter the full path at the beginning nor the version at the end.

Ensure your terminal is well configured:

####Windows
```
(workshops) C:\Users\lrekucki>where python
C:\Users\lrekucki\workshops\Scripts\python.exe


(workshops) C:\Users\lrekucki>where pip
C:\Users\lrekucki\workshops\Scripts\pip.exe


(workshops) C:\Users\lrekucki>python --version
3.6.0
```
####Linux or Mac

```
(workshops) ~$ which python
/home/lrekucki/workshops/bin/python
(workshops) ~$ which pip
/home/lrekucki/workshops/bin/pip

(workshops) ~$ python --version
3.6.0
```

###Note

You may already have the pip command available on your system. Always check which pip you are using with command: pip --version. You can do this by running one of these commands:

```
$ pip --version
$ pip3 --version
$ pip3.6 --version
```
It will give you the version of pip and a path to your virtual environment directory.

If you can’t find your pip or there is a problem after typing which pip (where pip on windows), you may need to reinstall pip:

```
$ python -m pip uninstall pip
$ python -m ensurepip
```

###Summary

####New virtual environment installation:

#####Windows
```
C:\Users\lrekucki> C:\Python34\python -m venv workshops
```


#####Linux or Mac
```
$ python3.6 -m venv workshops
```

####Virtual environment activation:

#####Windows
```
C:\Users\lrekucki> workshops\Scripts\activate
```
#####Linux or Mac
```
~$ source workshops/bin/activate
```
Just make sure that you use the proper Python version:
```
(workshops) ~$ python --version
3.6.0
```
